import React from 'react'
import './App.scss'
import Footer from './Components/Footer/Footer'
import Navbar from './Components/Navbar/Navbar'
import News from './Components/News/News'

export default function App() {
  return (
    <div>
      <Navbar/>
      <div className="jumbrotron"></div>
      <News/>
      <Footer/>
    </div>
  )
}

