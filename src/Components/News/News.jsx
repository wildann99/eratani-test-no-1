import React from 'react'
import './News.scss'
import img1 from "../../Assets/image 32.jpg"
import img2 from "../../Assets/image 7.jpg"
import img3 from "../../Assets/image 6.jpg"
import img4 from "../../Assets/image 4.jpg"
import img5 from "../../Assets/image2.jpg"
import img6 from "../../Assets/image1.jpg"

const data = [
  {
    id: 1,
    img: img1,
    title: "Lebih Menguntungkan, Petani Cirebon Pilih Tanam Bawang Merah Ketimbang Padi",
    by: "Muhammad Gilang",
    date: "28/10/2021"
  },
  {
    id: 2,
    img: img2,
    title: "Si Glowing, Bawang Merah Ramah Lingkungan yang Wanginya Menyengat",
    by: "Muhammad Gilang",
    date: "28/10/2021"
  },
  {
    id: 3,
    img: img3,
    title: "Panen Cabai Rawit di Temanggung Melimpah, Kebutuhan Idul Adha Aman",
    by: "Muhammad Gilang",
    date: "28/10/2021"
  },
  {
    id: 4,
    img: img4,
    title: "Tingkatkan Produktivitas Kapas dengan Teknologi Sambung Pucuk",
    by: "Muhammad Gilang",
    date: "28/10/2021"
  },
  {
    id: 5,
    img: img5,
    title: "50.000 Bibit Tanaman Purun untuk 1 Hektare di Tanam di 4 Desa Kabupaten Batola",
    by: "Muhammad Gilang",
    date: "28/10/2021"
  },
  {
    id: 6,
    img: img6,
    title: "Tanaman Porang dan Kebutuhan Pasar Global",
    by: "Muhammad Gilang",
    date: "28/10/2021"
  }
]


export default function News() {
  return (
    <div className='news'>
      <div className="news_container">
        <div className="news_title">
         Tips & Berita Pertanian > Berita Terbaru
        </div>

        <div className="list_news">
          {data.map(item => (
            <div className="list_news-card" key={item.id}>
              <img src={item.img} alt="" className='card_image'/>
              <div className="wrapper">
                <div className="inner-wrapper">
                  <div>TIPS</div>
                  <div>{item.date}</div>
                </div>
                <div className="title">{item.title}</div>
                <div className="author">Oleh <span>{item.by}</span></div>

                <button className="read">Baca Selengkapnya</button>
              </div>
            </div>
          ))}
         
        </div>
        <button className="see">Lihat Semua</button>
      </div>
    </div>
  )
}
