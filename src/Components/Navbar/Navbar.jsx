import React from 'react'
import "./Navbar.scss"
import logo from '../../Assets/Logo (1).png'
import menu from '../../Assets/Vector.svg'

export default function Navbar() {
  return (
    <div className='navbar'>
      <div className='navbar_container'>
        <img src={logo} alt="" />

        <ul className='navbar_list'>
          <li className='navbar_list-item'><a href="###">Beranda</a></li>
          <li className='navbar_list-item'><a href="###">Tentang kami</a></li>
          <li className='navbar_list-item'><a href="###">Tips & Berita Pertanian</a></li>
          <li className='navbar_list-item'><a href="###">Kegiatan</a></li>
        </ul>

        <button className="navbar_button">Mitra Petani</button>
       <button className="navbar_menu-mobile"> <img src={menu} alt="" /></button>
      </div>
    </div>
  )
}
