import React from "react";
import "./Footer.scss";
import logo from "../../Assets/logoputih.png";
import web from "../../Assets/web.svg";
import top from "../../Assets/top.svg";
import tiktok from "../../Assets/tiktok.svg";
import ig from "../../Assets/ig.svg";
import linked from "../../Assets/linked.svg";
import yt from "../../Assets/youtube.svg";
import wa from "../../Assets/wa.svg";

export default function Footer() {
  return (
    <div className="footer">
      <div className="footer_container">
        <div className="wrapper">
          <img src={logo} alt="" className="footer_logo" />
          <div className="top">
            <img src={top} alt="" />
          </div>
        </div>
        <div className="footer_content">
          <div className="flex-1">
            <div className="address">
              Jl. Casablanca Raya Kav 88, Kel. Menteng Dalam, Kec. Tebet, Gedung
              Pakuwon Tower Lt 26 Unit J, Jakarta Selatan, DKI Jakarta 12870,
              Indonesia
            </div>
            <div className="inner-wrapper">
              <div className="email">Email : info.eratani@eratani.co.id</div>
              <div className="tel ">Telephone : +62 811 952 2577</div>
              <div className="icon-wrapper">
                <img src={tiktok} alt="" />
                <img src={ig} alt="" />
                <img src={linked} alt="" />
                <img src={yt} alt="" />
                <img src={wa} alt="" />
              </div>
            </div>
          </div>
          <div className="flex-2">
            <div className="footer_menu">
              <div className="title">Menu</div>
              <ul>
                <li className="footer_list">
                  <a href="###">Tim Kami</a>
                </li>
                <li className="footer_list">
                  <a href="###">Mitra Eratani</a>
                </li>
                <li className="footer_list">
                  <a href="###">Tips & Berita Pertanian</a>
                </li>
                <li className="footer_list">
                  <a href="###">Karir</a>
                </li>
              </ul>
            </div>
            <div className="footer_lang">
              <img src={web} alt="" />
              <div>IN</div>
              <div>EN</div>
            </div>
          </div>
        </div>
        <div className="copyright">Copyright © 2021 by PT Eratani Teknologi Nusantara</div>
      </div>
    </div>
  );
}
